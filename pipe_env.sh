#!/bin/bash

print_help() {
  echo "Function to set CASA and PIPELINE environment variables.
The CASA path is appended to the beginning of PATH variable. The
PIPELINE path is stored in SCIPIPE_HEURISTICS variable.

Options:
   c    CASA path
   p    PIPELINE path
   h    help information

If function is called without arguments, then the default PATH and
SCIPIPE_HEURISTICS are restored."
}

restore_path() {

# Determine system or preset parameters
if [ -z "${PATH_ORIGIN}" ]
  then
    PATH_ORIGIN=${PATH}
fi

echo "Restoring PATH"

export PATH=${PATH_ORIGIN}

}

pipe_env() {

# Localise variables
local OPTARG
local OPTIND

# Get arguments
while getopts "c:p:h" option; do
	case "${option}" in 
		c) CASA_PATH=${OPTARG};;
		p) PIPE_PATH=${OPTARG};;
    h) print_help ; return ;;
	esac
done

# Determine system or preset parameters
restore_path

# Set new paths
export PATH=${CASA_PATH}:${CASA_PATH}/bin:${PATH}
# Assume that the ~/.casa/startup.py script is deployed and SCIPIPE_HEURISTICS
# environment variable is added to PYTHONPATH if it is available.
export SCIPIPE_HEURISTICS=${PIPE_PATH}

# Clean up
unset CASA_PATH
unset PIPE_PATH

# Inform user
echo "Setting up CASA-PIPELINE environment..."

}
