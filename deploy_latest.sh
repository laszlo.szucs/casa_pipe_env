#!/bin/bash
#
# Script to deploy pipeline version on remote host
#
# Set defaults
REMOTE_HOST=selene
# ${HOME} cannot be used in this case, as it would be interpreted on local machine,
# not on the target machine.
CASA_PATH_REMOTE="~/casa/casa-6.1.1-8"
PIPE_PATH_REMOTE="~/pipeline_src/latest"
FOLDER_TO_TAR="pipeline"

# Localise variables
local OPTARG
local OPTIND

# Get arguments
while getopts ":c:p:r:t:" option; do
	case "${option}" in 
		c) CASA_PATH_REMOTE=${OPTARG};;
		p) PIPE_PATH_REMOTE=${OPTARG};;
		r) REMOTE_HOST=${OPTARG};;
		t) FOLDER_TO_TAR=${OPTARG};;
    *) continue ;;
	esac
done

echo "Deploying ${FOLDER_TO_TAR} on remote ${REMOTE_HOST}"
echo ""
echo "CASA_PATH (remote): ${CASA_PATH_REMOTE}"
echo "PIPE_PATH (remote): ${PIPE_PATH_REMOTE}"
echo ""

# Create tar file from developement pipeline directory on host
echo "Creating tar archive..."
tar -cf "${FOLDER_TO_TAR}.tar" "${FOLDER_TO_TAR}"

# Clean developement pipeline directory on remote 
echo "Cleaning ${PIPE_PATH_REMOTE} on remote..."
ssh "${REMOTE_HOST}" bash -c "'rm -rf ${PIPE_PATH_REMOTE} ; mkdir ${PIPE_PATH_REMOTE}'"

# Copy tar file to remote
echo "Copying tar archive to remote..."
scp "${FOLDER_TO_TAR}.tar" "${REMOTE_HOST}:${PIPE_PATH_REMOTE}/."

# Untar pipeline and build interface on remote
echo "Begin deployment on remote..."
ssh "${REMOTE_HOST}" bash -c "'source ~/.bashrc ; pipe_env -c ${CASA_PATH_REMOTE} -p ${PIPE_PATH_REMOTE} ; which python3 ; cd ${PIPE_PATH_REMOTE} ; tar -xf ${FOLDER_TO_TAR}.tar ; mv ${FOLDER_TO_TAR} ${FOLDER_TO_TAR}_ ; mv ${FOLDER_TO_TAR}_/* . ; mv ${FOLDER_TO_TAR}_/.git . ; rm -rf ${FOLDER_TO_TAR}_ ; python3 setup.py buildmytasks -i'"

# Feedback to user
echo "DONE"
echo "${FOLDER_TO_TAR} is deployed on ${REMOTE_HOST}"
echo "Command to import new deployment on remote:"
echo "pipe_env -c ${CASA_PATH_REMOTE} -p ${PIPE_PATH_REMOTE}"
