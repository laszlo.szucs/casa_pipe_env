'''
CASA startup configuration file.

Should be placed to ~/.casa/ folder.
'''

# Disable telemetry
telemetry_enabled = False