"""
This is a pipeline developer CASA startup script.

It should be copied to the ~/.casa folder to take effect.
Optionally it can be paired with ~/.casa/config.py (e.g. to disable telemetry)
"""

import sys, os
from importlib import reload

if "SCIPIPE_HEURISTICS" in os.environ:
    print("Adding pipeline heuristics to python path")
    sys.path.insert(0, os.path.expandvars("$SCIPIPE_HEURISTICS"))
    # Initialise the pipeline
    import pipeline
    pipeline.initcli()
    import pipeline.infrastructure.executeppr as eppr
    import pipeline.recipereducer