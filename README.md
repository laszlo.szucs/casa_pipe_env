Script to set up CASA and Pipeline work environment

Content
=======

build_casa_pipe.sh
------------------
Obtains specified version of CASA and the Pipeline and sets up the $PATH 
environment variable appropriately.

Generates the following files:

 - .bash_casa_pipe		source this do add casa and pipe executables 
				        to $PATH.
 - .bash_restore_path	source this to restore $PATH to the state 
				        before .bash_casa_pipe was sourced.
				        
CASA configuration files
------------------------
 - .casa/config.py      CASA configuration for catching --pipeline 
                        argument and disabling telemetry
 - .casa/startup.py     CASA startup file, adds SCIPIPE_HEURISTICS to 
                        search path if set (needed for the pipe_env script)

pipe_env.sh
-----------
Script to set up CASA and pipeline search paths. Supersedes and simplifies 
build_casa_pipe.sh script. The config.py and startup.py files should be 
copied to the .casa folder manually for this to work.

deplot_latest.sh
----------------
Script to copy to and compile task interface on remote computer. Used to 
automate the deployment testing and development code.